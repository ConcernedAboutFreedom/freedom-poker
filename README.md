## THIS IS A MODIFIED VERSION OF POKERTH.
# Freedom-Poker
PokerTH is a poker game written in C++/Qt. Freedom Poker is a, forked, mostly 1-to-1 copy of PokerTH

Almost all data included in this work is entirely copied from PokerTH and will continue to be copied as needed for updates. The only parts that have been modified, as of the date listed below, are:

1. Any section(s) regarding bad word filters. It is not the job of a poker game to determine what is or is not appropriate language.
2. Any section(s) allowing the automated kicking, banning, or muting of a user. A person with a log should be the one to ban, kick or mute. Thus being able to be held accountable for wrong or inappropriate decisions
3. The about page has been modified to reflect the changes and make the user aware of them.

- An example of why these things are bad, if reasons even have to be given, is if tomorrow the creators decided that the word 'taco' should not be said by any user who lives in a European country. All they would have to do is add it to the list and have a machine ban you. No accountability, no recourse; no freedom.

Some things have also been added. All of which reside in the main folder and of course include this README. None of these additional items include any item which could interact with or modify the program or it's code

Freedom Poker retains the AGPL license from PokerTH and abides by that license. If you have found a way this fork DOES NOT comply with the AGPL, please go to our bitbucket page < https://bitbucket.org/ConcernedAboutFreedom/freedom-poker/src/master/ > and file a bug report or make any of the creators aware via private message via github. As the creators wish to stay anonymous, no other forms of communication will be given at this time.

Last modification date: September 4th, 2019 at ~1:00 AM PST

I am currently having difficulties compiling the program. I will upload what I have currently though in testing.